//API:part of server responsible for receving requests and sending responses
//Rest: need to separate iser interface concerns of the client from data storage concerns of the server
	//staleness: server does not need to know client state and vice versa
	//every client request has all the info it needs to be processed by the API
	//made possible via resources and HTTP methods
	//standardized communication: enables a decoupled server to understand, process and respond to client requests without knowing client state
		//implemented via resources represented as Uniform Resource Identified(URI) endpoints and HTTP methods
		 //resources are plural by convention (ex.api/photos)
	//anatomy of client request:operation to be performed dictated by HTTP methods
		//HTTP METHOD:
		//GET-receive
		//POST-create
		//PUT-update
		//DELETE-erase

		//CRUD OPERATION:
		//SELECT-receive
		//INSERT-create
		//UPDATE-update
		//DELETE-delete
//Express.js: an open-source, unopinionated web application framework written in JS and hosted in Node.js

/*First, we load the expressjs module into our application and saved it in a variable called express*/
const express = require("express");

/*Create an application with expressjs. 
This creates an application that uses express and stores in a variable called app. Which makes it easier to use expressjs methods in our api*/
const app = express();

/*Port is just a variable to contain the port number we want to designate for out new expressjs api*/
const port = 4000;

//middleware - will read a JSON data
app.use(express.json());

/*
to create a new route in expressjs, we first access from our express() package, our method. For get method request, access express (app), and use get()
	syntax: app.routemethod('/', {req, res}=>{
		//function to handle request and response
	})
*/
app.get('/', (req,res)=>{
	//res.send() ends the response to the client.
	//res.send() already does the res.writeHead() and res.end() automatically
	res.send("Hello World from our New ExpressJS api!");
});
app.get('/hello', (req,res)=>{
	res.send("Hello, Batch 157!");
});
/*app.post('/hello', (req,res)=>{
	console.log(req.body)
	res.send(`Hello there, ${req.body.firstname} ${req.body.lastname}`);
});*/
app.post('/hello', (req,res)=>{
	console.log(req.body)
	res.send(`Hello, I am ${req.body.name}, I am ${req.body.age}. I live in ${req.body.address}`);
});

let users = [
	{
		username:"johndoe",
		password:"johndoe1243"
	}


];

app.post('/signup',(req,res)=>{
	console.log(req.body)

	if(req.body.username != '' && req.body.password !=''){
		users.push(req.body);
		console.log(users);
		res.send(`User ${req.body.username} successfully registered.`)
	} else{	
		res.send("Please input BOTH username and password")
	}

});

app.put("/change-password", (req,res)=> {
	
	//initialize only, different from message="";-> string
	let message;

	//create loop to search element inside user array  or find() method
	for (let i = 0; i< users.length; i++){
		if(req.body.username === users[i].username){

			users[i].password = req.body.password;
			message = `User ${req.body.username}'s password has been updated.`
			break;
		} else {
			message = `User does not exist.`
		}
	}
	res.send(message)
});

//SESSION 29 ACTIVITY
app.get("/home", (req,res)=>{
	res.send(`Welcome to homepage`)
});
app.get("/users",(req,res)=>{
	res.send(users)
});
app.delete("/delete-user", (req,res)=>{
	users.length=users.length-1;
	res.send(`User ${req.body.username} has been deleted`)
});






/*app.listen() allows us to designate the correct port to our new expressjs api and once the server is running we can then run a function that runds a console.log() which contains a message that the server is running*/
app.listen(port,()=>{console.log(`Server is running at port ${port}`)});